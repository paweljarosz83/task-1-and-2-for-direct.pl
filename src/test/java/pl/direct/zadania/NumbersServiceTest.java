package pl.direct.zadania;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import pl.direct.zadania.numbers.NumbersService;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
public class NumbersServiceTest {

	@Autowired
	private NumbersService service;
	
	private static int ZERO = 0;
	private static int ONE = 1;
	
	@BeforeAll
	public static void setup(){
		int[] testSample = {0,1,54,1,9,32,0,1,1111,4};
		NumbersService.setNUMBERS(testSample);
	}
	
	@Test
	public void shouldWorkForFirstTwo() {
		assertEquals(ONE, service.getValue(2));
	}
	
	@Test
	public void shouldWorkForLastTwo() {
		assertEquals(1112, service.getValue(9));
	}
	
	@Test
	public void shouldFailForWrongResult() {
		assertNotEquals(ZERO, service.getValue(3));
	}
	
	@Test
	public void shouldThrowAnExceptionForNegative() {
		assertThrows(IllegalArgumentException.class,()->service.getValue(-1),"Exception expected");
	}
	
	@Test
	public void shouldThrowAnExceptionForZero() {
		assertThrows(IllegalArgumentException.class,()->service.getValue(ZERO),"Exception expected");
	}
	
	@Test
	public void shouldThrowAnExceptionForArrayOutOfBoundsLeft() {
		assertThrows(IllegalArgumentException.class,()->service.getValue(ONE),"Exception expected");
	}
	
	@Test
	public void shouldThrowAnExceptionForArrayOutOfBoundsRight() {
		assertThrows(IllegalArgumentException.class,()->service.getValue(99999),"Exception expected");
	}
}
