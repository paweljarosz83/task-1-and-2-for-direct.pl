package pl.direct.zadania;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZadaniaDirectPl3Application {

	public static void main(String[] args) {
		SpringApplication.run(ZadaniaDirectPl3Application.class, args);
	}

}
