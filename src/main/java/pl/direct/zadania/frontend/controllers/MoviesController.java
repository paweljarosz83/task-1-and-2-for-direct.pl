package pl.direct.zadania.frontend.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MoviesController {
	
	@RequestMapping("/")
	public String index() {
		return "index";
	}
}
