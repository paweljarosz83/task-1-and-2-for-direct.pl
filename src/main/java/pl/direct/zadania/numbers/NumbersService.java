package pl.direct.zadania.numbers;

import java.security.InvalidParameterException;

import org.springframework.stereotype.Service;

@Service
public class NumbersService {


	private static int[] NUMBERS = {1,1,2,3,5,8,13,21,31};
	

	public int getValue(int n){
		
		if(!validateInput(n)){
			throw new InvalidParameterException("Fix input");
		}else{
			return NUMBERS[n-1]+NUMBERS[n-2];
		}
	}
	
	private boolean validateInput(int n) {
		
		if(n<2) {
			return false;
		}else if(n<=0){
			return false;
		}else if(n>NUMBERS.length){
			return false;
		}else{
			return true;
		}
	}

	public static int[] getNUMBERS() {
		return NUMBERS;
	}

	public static void setNUMBERS(int[] nUMBERS) {
		NUMBERS = nUMBERS;
	}
}


