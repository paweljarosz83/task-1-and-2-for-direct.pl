$(document).ready(function() {

		let moviesArray;
		
		$.getJSON('js/data.json', function (data) {

		moviesArray = data.Movies;
		
		$("#moviesTable").DataTable({
		'autoWidth': false,
		'responsive': false,
		'dom': 'Rlfrtip',
		'order': [[ 0, 'desc' ]],
		'data':moviesArray,
		'columns':[
		           { "data": "Title" },
		           { "data": "Year" },
		           { "data": "Rated" },
  				   { "data": "Released" },
		           { "data": "Runtime" },
  				   { "data": "Country" },
		           { "data": "Writer" },
		           { "data": "Genre" },
  				   { "data": "Director" },
          		   { "data": "imdbID",
		        	   "render": function(data,type,row,meta){
		        		   return "<button class='showMore' id='"+data+"'>More</button>";
		        	   }
		           },
		          
		           ]
		});
	});
	
	
	
	
	$(document).on('click','.showMore',function(e){
		
		setupModal(findObjectInJson(this.id));
		$("#movieDetails").modal();
	});
	

	function findObjectInJson(id){
		
		for (var i=0 ; i < moviesArray.length ; i++){
        	
    		if (moviesArray[i].imdbID === id) {
				return moviesArray[i];
    		}
		}
	}
	
	function setupModal(selectedMovie){
		
		$("#actors").html(selectedMovie.Actors);
		$("#language").html(selectedMovie.Language);
		$("#country").html(selectedMovie.Country);
		$("#awards").html(selectedMovie.Awards);
		$("#metascore").html(selectedMovie.Metascore);
		$("#imdbRating").html(selectedMovie.imdbRating);
		$("#imdbVotes").html(selectedMovie.imdbVotes);
		$("#imdbID").html(selectedMovie.imdbID);
		$("#type").html(selectedMovie.Type);
		$("#response").html(selectedMovie.Response);
		$("#poster").attr('src', selectedMovie.Poster);
		$("#plot").html(selectedMovie.Plot);
	}
	
	
	
	
});